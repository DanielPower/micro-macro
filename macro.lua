-- Execute macro script and get the resulting table of commands
local macro = require("macros/"..arg[1])()

local trackSleep = false														-- Don't track sleepTime until the time command is called
local totalSleep = 0															-- Total amount of time slept so far
local sleepTime = 0.05															-- Amount of time (seconds) to sleep between inputs (Default: 0.05)
local sleepfile = io.open("sleep.txt", "w")										-- File to output with the total sleep time

for _, key in ipairs(macro) do													-- Iterate through each item in the table of commands
	if key == "trackSleep" then
		trackSleep = true														-- We only begin tracking sleep time after this command is read
	elseif key:sub(1, 4) == "type" then											-- If the command starts with the word "type", then read it as a series of one character inputs, rather than a single input
		local chars = {}
		for i=1, #key:sub(6) do													-- Ignore the "type " at the beginning of the command to get the characters
			local char = key:sub(5+i, 5+i)										-- Get the current character in the type command
			-- This should ideally be replaced with a lookup table for all possible special characters. So far I've just been manually adding each one as I've needed it
			if char == " " then table.insert(chars, "space")
			elseif char == "." then table.insert(chars, "period")
			elseif char == "/" then table.insert(chars, "slash")
			elseif char == "|" then table.insert(chars, "bar")
			elseif char == "-" then table.insert(chars, "minus")
			else table.insert(chars, char) end									-- Add the chracter to the chars table
		end
		for _, char in ipairs(chars) do
			os.execute("xdotool key "..char)									-- Input each character individually, one by one.
			os.execute("sleep "..sleepTime)										-- We sleep between inputs, because some inputs will get missed if we make them too quickly
			if trackSleep then totalSleep = totalSleep + sleepTime end			-- We keep track of the time spent sleeping, so we can remove it from our benchmark result
		end
	else
		if key:sub(1, 4) == "hold" then
			os.execute("xdotool keydown "..char)								-- Hold a key until manually released
		elseif key:sub(1, 7) == "release" then
			os.execute("xdotool keyup "..char)									-- Release a key that is being held
		else
			os.execute("xdotool key "..key)										-- Simply press a key
		end
		os.execute("sleep "..sleepTime)
		if trackSleep then totalSleep = totalSleep + sleepTime end
	end
end

sleepfile:write("Slept for "..totalSleep.." seconds.")							-- Write to a file so you can see how much time was slept during execution. This time should be subtracted from benchmark results.
