return function()
	-- Usage -> lua ./macro.lua searchandreplace [micro binary] [test file] [search string] [replace string] &
	-- Don't forget to include the ampersand at the end of the command, or the program will not properly receive input
	local binary = arg[2]
	local file = arg[3]
	local search = arg[4]
	local replace = arg[5]

	-- Check input
	assert(binary and file and search and replace, "Not enough arguments")

	local macro = {
		'type time '..binary..' '..file,
		'Return', -- Run micro with the given file
		'trackSleep', -- Micro is now running with the "time" command, so we will now begin tracking sleepTime
		'Control_L+e', -- Open Micro's command pallatte
		'type replace '..search..' '..replace, -- Type the command to search for and replace the given arguments
		'Return', -- Execute the command
		'Control_L+q', -- Quit
		'n', -- Do not save changes
	}

	return macro
end
