return function()
	-- Usage -> lua ./macro.lua searchandreplace [micro binary] [file] &
	-- Don't forget to include the ampersand at the end of the command, or the program will not properly receive input
	local binary = arg[2]
	local file = arg[3]

	assert(#arg == 3, "Not enough arguments")

	local macro = {
		'type time '..binary..' '..file,
		'Return',
		'trackSleep',
		'Control_L+q'
	}

	return macro
end
