# Results
Testing based on Joe's Own Editor comparisons  
https://github.com/jhallen/joes-sandbox/tree/master/editor-perf

## Memory Usage when loading hello.c
| Version | Memory Usage | Standard Deviation |
| --- | --- | --- |
| Micro 1.1.4 | 21.4MB | 1.16 |
| Micro 1.1.5-138 | 25.7MB | 1.44 |
| Micro 1.1.5-142 | 31.4MB | 2.04 |

## Memory Usage when loading test.xml
| Version | Memory Usage | Standard Deviation |
| --- | --- | --- |
| Micro 1.1.4 | 28.8MB | --- |
| Micro 1.1.5-138 | 59.0MB | --- |
| Micro 1.1.5-142 | 63.6MB | 1.68 |

## Time to load test.xml, jump to end of file and exit

| Version | Time (seconds) | Standard Deviation |
| --- | --- | --- |
| Micro 1.1.4 | 0.168 | 0.001 |
| Micro 1.1.5-138 | 0.378 | 0.002 |
| Micro 1.1.5-142 | 0.377 | 0.003 |

## Simple Search and Replace
Load test.xml, replace "thing" with "thang" and then exit.

| Version | Time (seconds) | Standard Deviation |
| --- | --- | --- |
| Micro 1.1.4 | !!! | !!! |
| Micro 1.1.5-138 | !!! | !!! |
| Micro 1.1.5-142 | 0.258 | 0.001 |

While 1.1.4 and 1.1.5-138 were so slow to complete this task that I never had
the patience for them to finish (10+ minutes), thanks to commit
`007b060cbd42ca75c1da8e799592c2e749ede901`, micro 1.1.5-142 completes this task
very quickly.

## Regex Search and Replace
Load test.xml and replace 100|200 with EXACT and then exit

| Version | Time (secodns) | Standard Deviation |
| --- | --- | --- |
| Micro 1.1.4 | 4.619 | 0.049 |
| Micro 1.1.5-138 | !!! | !!! |
| Micro 1.1.5-142 | 0.868 | 0.008 |

## Load a file with very long lines
Load longlines.txt and then exit

| Version | Time (seconds) | Standard Deviation |
| --- | --- | --- |
| Micro 1.1.4 | !!! | !!! |
| Micro 1.1.5-138 | 0.133 | 0.001 |
| Micro 1.1.5-142 | 0.134 | 0.001 |
